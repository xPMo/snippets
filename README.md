# Snippets

Various scripts or other programs I've written, which I want to keep public.

If something isn't here, then there's a good chance I've added it to
[my own dotfiles](https://gitlab.com/gamma-neodots)
