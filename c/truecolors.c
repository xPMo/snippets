#include <stdint.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>

int print_colorspace(unsigned short rows, unsigned short cols, int shift)
{
	rows -= 1;
	printf("\e[48;2;0;0;0m%*s\e[0m\n", cols, "");
	for (uint64_t y = 1; y < rows; y++) {
		for (uint64_t x = 0; x < cols; x++) {
			uint64_t x1 = (x + shift) % cols;
			uint64_t x2 = 0x1000 * ( (6 * x1) % cols) / cols;
			uint64_t r, g, b;
			switch ((6 * x1) / cols) {
			case 0:
				r = 0xff0;
				g = x2;
				b = 0;
				break;
			case 1:
				r = 0xff0 - x2;
				g = 0xff0;
				b = 0;
				break;
			case 2:
				r = 0;
				g = 0xff0;
				b = x2;
				break;
			case 3:
				r = 0;
				g = 0xff0 - x2;
				b = 0xff0;
				break;
			case 4:
				r = x2;
				g = 0;
				b = 0xff0;
				break;
			//case 5:
			default:
				r = 0xff0 ;
				g = 0;
				b = 0xff0 - x2;
				break;
			}
			uint64_t y2 = 0x1000 * ( (2 * y) % rows) / rows;
			switch ((2 * y) / rows){
			case 0:
				r = r * y2 / 0x10000;
				g = g * y2 / 0x10000;
				b = b * y2 / 0x10000;
				break;
			case 1:
				 r = (0x1000 * (r + y2) - y2 * r) / 0x10000;
				 g = (0x1000 * (g + y2) - y2 * g) / 0x10000;
				 b = (0x1000 * (b + y2) - y2 * b) / 0x10000;
				 break;
			}
			printf("\e[48;2;%" PRIu64 ";%" PRIu64 ";%" PRIu64 "m ", r, g, b);
		}
		puts("\e[0m");
	}
	printf("\e[48;2;255;255;255m%*s\e[0m\n", cols, "");
	return 0;
}

int main(int argc, char ** argv)
{
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	unsigned short cols =  w.ws_col;
	unsigned short rows =  w.ws_row;
	int shift = 0;
	switch (argc) {
	case 4: shift = atol(argv[3]); /* fallthrough */
	case 3: cols  = atoi(argv[2]); /* fallthrough */
	case 2: rows  = atoi(argv[1]);
	}

	if (cols == 0 || rows == 0) {
		fprintf(stderr, "Usage: %s [ rows [ columns [ shift ] ] ]\n", argv[0]);
		return 1;
	}
	return print_colorspace(rows, cols, shift);
}
