#include <stdio.h>
#include <stdlib.h>

static inline void ends(int *i, int n)
{
	for (; *i <= n; (*i)++) {
		if (*i % 3 == 0) {
			puts("Fizz");
		} else if (*i % 5 == 0) {
			puts("Buzz");
		} else {
			printf("%d\n", *i);
		}
	}
}

int main(int argc, char **argv)
{
	int n = 100;
	int i = 1;
	switch (argc) {
	case 3: i = atoi(argv[2]); /* FALLTHROUGH */
	case 2: n = atoi(argv[1]); /* FALLTHROUGH */
	}
	if ((i - 1) % 15){
		int i2 = i + 14 - i % 15;
		ends(&i, i2);
		puts("FizzBuzz");
	}
	for (; i <= n - 14; i += 15) {
		printf("%d\n%d\nFizz\n%d\nBuzz\n"       //  1- 5
				"Fizz\n%d\n%d\nFizz\nBuzz\n"    //  6-10
				"%d\nFizz\n%d\n%d\nFizzBuzz\n", // 11-15
				i   , i+ 1, /*F*/ i+ 3, /* B*/
				/*F*/ i+ 6, i+ 7, /*F*/ /* B*/
				i+10, /*F*/ i+12, i+13  /*FB*/);
	}
	ends(&i, n);
}
