#!/bin/sh
case $1 in
-h | -help | --help)
	echo "Usage: $0 [ convert args ... ]

A script which gets the active outputs
and locks the screen based on the 'convert' filter provided.

Each output's grim + convert instance is run in parallel

Examples:

	# scale-based pixelate
	$0 -scale 20x20% -scale 500x500%

	# sample-based pixelate and desaturate [ DEFAULT ]
	-sample 20x20% -modulate 100,50 -sample 500x500%

	# blur
	$0 -blur 2x8
"
	exit 0
	;;
esac

[ -z "$1" ] || set -- -sample 20x20% -modulate 100,50 -sample 500x500%

dir="$(mktemp -d)"
trap '{ rm -r "${dir?}"; return $?; }' INT
swaymsg -t get_outputs | jq -r '.[]|select(.active).name' | {
	while read -r output; do
		# ppm is a bitmapped format supported by grim, convert, and swaylock
		img="$(mktemp -p "$dir" --suffix=.ppm)"
		(
			grim -o "$output" "$img"
			convert "$img" "$@" "$img"
		) &
		lock_args="--image=$output:$img
$lock_args"
	done
	wait

	# suppress globbing, use newlines to split swaylock args
	set -f
	#shellcheck disable=2086
	IFS='
' swaylock $lock_args
}
