urlencode() {
	local length="${#1}"
	for ((i = 0; i < length; i++)); do
		local c="${1:$i:1}"
		case $c in
		[a-zA-Z0-9.~_-]) printf %s "$c" ;;
		*)
			printf '%s' "$c" | xxd -p -c1 | while read -r c; do
				printf '%%%s' "$c"
			done
			;;
		esac
	done
}
terminal-link() {
	local encoded
	encoded="$(urlencode "$2")"
	printf '\e]8;;%s\a%s\e]8;;\a' "${encoded}" "${1}"
}
