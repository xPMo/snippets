#!/usr/bin/env sh

IFS='|' # zenity uses '|' to seperate values

parse() {
	printf 'First: %s\nSecond: %s\n' "$1" "$2"
}

#shellcheck disable=2046
parse $(zenity --forms --add-entry="First" --add-entry="Second")
