#!/usr/bin/env bash
# https://redd.it/e0x2er
demo() {
	mkdir -p bin/{a,b,b/c}/v{1,2}
	touch bin/{a,b/c}/v{1,2}/{scala,java}{1,2}.{jar,xml}
	touch bin/{b,b/c}/v1/{a..c}.py
	# show current tree
	tree bin
}

case ${1//-/} in
demo) demo ;;
drydemo)
	mv() { echo "mv $*"; }
	demo
	;;
dry*) mv() { echo "mv $*"; } ;;
run) ;;
*)
	echo "Run as '$0 dry-demo' in an empty directory to run a demo, dry run"
	echo "Run as '$0 demo' in an empty directory to run a demo"
	echo "Run as '$0 dry-run' to run with the current directory structure, dry run"
	echo "Run as '$0 run' to run with the current directory structure"
	exit 0
	;;
esac

shopt -s globstar
declare -A matches=(
	['scala*.jar']=scala ['scala*.xml']=scala
	['java*.jar']=java ['java*.xml']=java
	['*.py']=python
)
for artifact in bin/**/v*/*; do
	for key in "${!matches[@]}"; do
		# [[ = ]] does pattern matching
		#shellcheck disable=2053
		if [[ ${artifact##*/} = $key ]]; then
			mkdir -p "${artifact%/*}/${matches[$key]}"
			mv "$artifact" "${artifact%/*}/${matches[$key]}"
			break
		fi
	done
done
