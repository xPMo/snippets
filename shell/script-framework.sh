#!/usr/bin/env bash
# https://redd.it/ckw0tk

cat >scriptfile <<EOF
the contents of your file before your array
however you'll need to escape dollar signs as \$

Tabs (as in the actual character you
use in your editor) will be preseved
SOME_ARRAY = [ 'somestring',<tab>'another string',<tab>'$1' ]

the rest of the script file
is here
EOF

interpreter ./scriptfile
