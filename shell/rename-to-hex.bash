#!/usr/bin/env bash
# https://redd.it/bbt8ud
shopt -s globstar
for d in **/; do
	pushd "$d" >/dev/null || continue
	i=0
	for f in *; do
		if [[ -f "$f" ]]; then
			mv -n "$f" "$(printf '%04x' "$i").${f##*.}"
			((i++))
		fi
	done
	popd >/dev/null || exit
done
