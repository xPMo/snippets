#!/usr/bin/env bash
# https://redd.it/fjszj6
shopt -s globstar
for dir in "${1:?}/"**/; do
	find "$dir" -maxdepth 1 -type f -printf '%T@ %p\0' |
		sort -rnz |
		sed -nz '2,$s/[^ ]* //p'
done | xargs -0 rm --
