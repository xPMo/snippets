#!/usr/bin/env sh
ws=$(i3-msg -t get_workspaces | jq -r '.[] | select(.focused).name')
i3-msg -t get_tree | jq -r "
	recurse(.nodes[])
	| select(.type==\"workspace\")
	| select(.name==\"$ws\")"
