#!/bin/bash
# https://redd.it/b7nooz
# use with mpd-host-toggle.sh

MPD_HOST_FILE="${XDG_RUNTIME_DIR:-/tmp}/current-mpd-host"
MY_MPD_HOST="$(<"$MPD_HOST_FILE")"
if [ -z "$MY_MPD_HOST" ]; then
	MY_MPD_HOST=127.0.0.1
	echo 127.0.0.1 >"$MPD_HOST_FILE"
fi
unset MPD_HOST_FILE
