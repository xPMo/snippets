#!/usr/bin/env bash
# view script before running
xsel -b
printf >&2 '\nOkay to run? [y/N]'
read -r -N 1 y
[[ "${y,}" = y ]] && xsel -b | exec bash
