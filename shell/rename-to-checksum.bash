#!/usr/bin/env bash
# https://redd.it/b19f3p
shopt -s globstar dotglob
for f in /source/dir/**; do
	[[ -f "$f" ]] || continue
	new="$(sha256sum "$f")"
	ext="${f##*.}"
	new="${new%% *}.${ext,,}"
	cp --reflink=auto -n "$f" "/dest/dir/$new"
done
