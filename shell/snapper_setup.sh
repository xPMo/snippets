#!/usr/bin/env sh
# don't keep ancient snapshots
sudo snapper set-config 'TIMELINE_LIMIT_YEARLY=0 TIMELINE_LIMIT_MONTHLY=0 TIMELINE_LIMIT_WEEKLY=2'
sudo snapper set-config 'NUMBER_LIMIT=30'
# cleanup (automatic cleanup may already be enabled)
sudo snapper cleanup timeline
sudo snapper cleanup empty-pre-post
sudo snapper cleanup number
# Setup automatic cleanup (this may already be enabled)
sudo systemctl start snapper-cleanup.timer
sudo systemctl enable snapper-cleanup.timer
