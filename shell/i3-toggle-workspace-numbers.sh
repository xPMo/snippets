#!/usr/bin/env sh
# bind "exec --no-startup-id $this $config_file" in i3
sed '/strip_workspace_numbers/{s/true/placeholder/; s/false/true/; s/placeholder/false}' -i "$1" &&
	i3-msg reload # need to reload _after_ sed exits
