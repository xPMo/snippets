#!/usr/bin/env zsh
zmodload zsh/zutil

string-replace(){
	emulate -L zsh
	setopt extendedglob

	local -a input method
	zparseopts -D -E -F - v+:=input \
		{p,-pattern,F,-fixed-string{,s}}=method
	
	
	# remove first -- or -
	local -i end_opts=$@[(i)(--|-)]
	set -- "${@[0,end_opts-1]}" "${@[end_opts+1,-1]}"

	if ! (($#input)); then
		typeset -g REPLY=$1
		shift
		input=(-v REPLY)
	fi
	if (($# & 1)); then
		print -u2 "Expected an even number of arguments, got $#"
		return 1
	fi

	# resolve keys as patterns?
	local pre post
	case ${method} in
		-p|--pat*) repl='(#m)(${(j<|>k)~map})/${map[(k)$MATCH]-$MATCH}' ;;
		*)         repl='(#m)(${(~j<|>k)map})/${map[$MATCH]-$MATCH}' ;;
	esac

	local -A map=("$@")

	local name
	for _ name in "${(@)input}"; do
		case ${(tP)name} in
			*-local)
				print -lu2 "$name is local to this function, cannot replace"
				return 1
				;;
			s*) eval typeset -g '$name=${(P)name//'$repl'}' ;;
			a*) eval typeset -g '$name=("${(@kvP)name//'$repl'}")' ;;
		esac
	done
}

string-replace "$@"
print -r "$REPLY"
