#!/usr/bin/env bash
# Automatically mute Spotify in Pulseaudio
# if it reports a trackid matching "spotify:ad:*"
spotify "$@" &
pid=$!
sleep 5
while
	si="$(pacmd list-sink-inputs | awk '/index:/{si = $2}; /"Spotify"/{print si; exit}')"
	if [[ -n "${si:-}" ]]; then
		playerctl --player spotify --follow metadata mpris:trackid |
			while read -r id; do
				if [[ $id == spotify:ad:* ]]; then
					pactl set-sink-input-mute "$si" 1
				else
					pactl set-sink-input-mute "$si" 0
				fi
			done
	fi
	echo >&2 "Unable to find a sink-input for Spotify."
	sleep 60
do :; done &
wait "$pid"
s=$?
setsid kill -- -$$
exit "$s"
