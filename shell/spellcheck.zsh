#!/usr/bin/env zsh
spellcheck () {
	dict=(${$(</usr/share/dict/usa)})
	for line in "${(@f)$(<$1)}"; do
		for word in ${=${line//[^[:alpha:]]/ }}
			printf '%s ' ${${${:-$dict[(r)$word:l]$dict[(r)$word]}:+$word}:-$'\e[31m'$word$'\e[0m'}
		echo
	done
}
spellcheck_eff(){
	#setopt localoptions extendedglob
	dict=(${$(</usr/share/dict/usa)})
	for line in "${(@f)$(<$1)}"; do
		for word in ${=${line//[^[:alpha:]]/ }}; do
			# binary search
			local -i n=$(($#dict / 2))
			local -i cur=$n
			while [[ $dict[cur] != (${word:l}|$word) ]] && ((n > 1 && (n = (n + 1) / 2))); do
				if [[ ${${dict[cur]}:l} > ${word:l} ]]; then
					((cur -= n))
				else
					((cur += n))
				fi
			done
			printf '%s ' ${${${${dict[cur]}:#($word|$word:l)}:+$'\e[31m'$word$'\e[0m'}:-$word}
		done
		echo
	done
}
[[ $- = *i* ]] || spellcheck_eff $@
