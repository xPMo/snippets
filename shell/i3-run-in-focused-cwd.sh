#!/bin/sh
#https://redd.it/uywtqz
for cmd in i3-msg swaymsg; do
	type "$cmd" >/dev/null 2>&1 && break
done || exit
focused=$("$cmd" -t get_tree | jq '.. | select(.focused)? | .pid')
cd -P /proc/$(pgrep -P "$focused" | head -n 1)/cwd
exec "$@"
