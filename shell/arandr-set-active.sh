#!/usr/bin/env sh
set -e # exit if cd or zenity fail
cd "$HOME/.screenlayout"
ln -sf "$(ls -- *.sh | zenity --list --column 'Layout' --text='Please choose a layout')" .active
. ./.active.sh
