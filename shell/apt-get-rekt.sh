#!/usr/bin/env bash
# POSIX sh can't name functions with '-'
apt-get() {
	if [ "$1" = 'rekt' ]; then
		paplay '/usr/local/share/audio/airhorns.wav'
	else
		command apt-get "$@"
	fi
}
