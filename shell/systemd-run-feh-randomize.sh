#!/usr/bin/env sh
# Put "exec --no-startup-id $this" in your i3 config
systemd-run --on-calendar="*-*-* *:*/5:00" --user -- \
	/usr/bin/feh --no-fehbg --bg-scale --randomize -r "$1"
