#!/usr/bin/env bash
# https://redd.it/b339sx
declare -a tmpfiles
tmpf() {
	while (($#)); do
		tmpfiles+=("$(mktemp)")
		eval "$1=\"${tmpfiles[-1]}\""
		shift 1
	done
}
trap 'rm "${tmpfiles[@]}"' EXIT INT # delete all temp files on exit

# Now when you need a file/var to store the output of a script:
tmpf x y # will set x and y to the paths of new temp files
#shellcheck disable=2154
openssl cmd ... >"$x"
...
# read in the file
#shellcheck disable=2154
openssl cmd ... <"$y"
