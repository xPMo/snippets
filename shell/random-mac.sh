#!/bin/sh
# https://redd.it/c0pbog
rand="$(tr -dc '0123456789abcdef' </dev/urandom | head -c 6)"
randEnd="${rand#??}"
macSpoofed="$(shuf -n1 OUI.list):${rand%????}:${randEnd%??}:${randEnd#??}"

# Better method, using only the randomness you need and abusing word splitting:

# shellcheck disable=2046
set -- $(head -c 3 </dev/urandom | hexdump -e '1/1 "%.2x "')
macSpoofed="$(shuf -n1 OUI.list):$1:$2:$3"

use "$macSpoofed"
