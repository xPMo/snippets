#!/usr/bin/env bash
# Automatically mute Spotify (or another player) in Pulseaudio
# if it reports a track length less than some value
player="spotify"
si_name="\"Spotify\""
repeat=0
mutelen=60
while getopts 'i:lp:s:t:h' opt; do
	case "$opt" in
	i) si="$OPTARG" ;;
	l) repeat=1 ;;
	p) player="$OPTARG" ;;
	s) si_name="$OPTARG" ;;
	t) mutelen="$((OPTARG))" ;;
	h) nr=1 ;&
	*)
		cat >&2 <<EOF
Usage: $(basename "$0") [ -t num ] [ -p name ] [ -s name ] [ -h ]

	-t num    below this length (in seconds) consider the
	          currently playing track to be an advertisment (default: 60)
	-p name   the dbus player name (default: spotify)
	-s name   the string to match sink-input on (default: \"Spotify\")
	-i num    the index of the sink-input to mute (default: parsed from pacmd dump)
	-l        loop forever instead of exiting if player is not found
EOF
		exit $((1 - nr))
		;;
	esac
done
[[ -z "${si:-}" ]] && si="$(pacmd list-sink-inputs | awk '/index:/{si = $2}; /'"$si_name"'/{print si; exit}')"
[[ -z "${si:-}" ]] && {
	echo >&2 "Unable to find a sink-input for Spotify."
	exit 1
}
while
	playerctl --player "$player" --follow metadata mpris:length |
		while read -r length; do
			if (((length / 1000000) > mutelen)); then
				pactl set-sink-input-mute "$si" 0
			else
				pactl set-sink-input-mute "$si" 1
			fi
		done
	((repeat))
do sleep 60; done
