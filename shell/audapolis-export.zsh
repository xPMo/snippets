#!/usr/bin/env zsh

## Usage: ./audapolis-export.zsh [input-project.audapolis] [ directory ]
## 
## If directory is given, output will be in directory/directory.wav
## Otherwise, output will be in input-project/input-project.wav

set -- ${1:?Missing input file} ${2:-${1%.*}}
[[ -d $2 ]] || {
	unzip -d $2 $1
}
cd $2
[[ -f ${2:t}.wav ]] && rm ${2:t}.wav

# pull timestamps and sources out of document.json
jq -r < document.json '.content[].content[] | "\(.source) \(.sourceStart) \(.length)"' |
{ {
	typeset -ga segments
	typeset -F 2 cur len next=-1.0
	integer=count
	while read -r src cur len; do
		# Are the segments contiguous in the same file? If not:
		if ((cur - next > 0.1)) || [[ $prev_src != $src ]]; then
			# end previous timestamp (-to), start next (-i, -ss)
			segments+=(-to $next -i sources/$src -ss $cur)
			prev_src=$src
		fi
		((next = cur + len))
	done

	# remove '-to -1.0', add '-to [last]'
	segments=(${segments:2} -to $next)
	integer -Z 3 count
	# every 6 elements:
	# -i [file] -ss [start] -to [end]
	for ((count = 0; count * 6 < $#segments; count++)); do
		files+=($count.wav)
		ffmpeg ${segments:count*6:6} -c copy $count.wav &
	done
	wait

	# concat demuxer needs quoted full paths as input file
	ffmpeg -f concat -safe 0 -i <(<<<${(F):-"file ${(@qq)^files:A}"}) -c copy ${2:t}.wav
} always {
	# cleanup
	(($#files)) && rm $files
} }
