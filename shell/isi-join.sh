#!/usr/bin/env bash
ddl=sample/isi-devices-drive-list
fas=sample/isi-for-array-space
nodes=sample/isi-nodes
awk '
# lnn location device lnum state
match($0, /([0-9]*)\s*(Bay\s*[0-9]*)\s*([^[:space:]]*)\s*([^[:space:]]*)\s*(.*)/, matches) {
	# do not process empty or journal state
	if (tolower(matches[5]) ~ /empty|journal/){
		next
	}

	key = matches[1] ":" matches[4]
	location[key] = matches[2]
	device[key]   = matches[3]
}


# lnn ldnum blkfree 
match($0, /^Cowboy-([0-9]*).*ldnum=([0-9]*),\s*blkfree=([0-9]*),\s*totalblk=([0-9]*),\s*usedino=([0-9]*),\s*inofree=([0-9]*),\s*totalino=([0-9]*)/, matches) {
	key = matches[1] ":" matches[2]
	blkfree[key]  = matches[3]
	totalblk[key] = matches[4]
	usedino[key]  = matches[5]
	inofree[key]  = matches[6]
	totalino[key] = matches[7]
}

/[0-9]* [0-9]*/{
	nodes[$1] = $2
}


END {
	for (key in blkfree) {
		split(key, ll, ":")
		print ll[2], blkfree[key], totalblk[key], (100 - 100 *  blkfree[key] / totalblk[key]), \
			location[key], ll[1], nodes[ll[1]]
	}
}
' "$ddl" "$fas" "$nodes" | sort -nr -k4 | column -t
