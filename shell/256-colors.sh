#!/usr/bin/env bash
shopt -s checkwinsize
(:;:) # microsleep
declare -i cols w
set_w_c() {
	local -i i=1
	for (( ; cols = $1 / i, COLUMNS / cols < 3 && i < $1; i++)); do :; done
	((w = COLUMNS / cols))
}
set_w_c 8
for i in {0..15}; do
	printf "\\e[48;5;${i}m%${w}s" "$i "
	((cols - i % cols == 1)) && printf '\e[0m\n'
done
set_w_c 6
for i in {16..231}; do
	printf "\\e[48;5;${i}m%${w}s" "$i "
	((cols - (i - 16) % cols == 1)) && printf '\e[0m\n'
done
set_w_c 12
for i in {232..255}; do
	printf "\\e[48;5;${i}m%${w}s" "$i "
	((cols - (i - 232) % cols == 1)) && printf '\e[0m\n'
done
