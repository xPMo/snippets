#!/usr/bin/env bash
case $1 in
-h | -help | --help)
	echo "Usage: $0 [ OUTPUT [ convert args ... ] ] [[ -- OUTPUT ... ]]

A script which gets the active outputs, screenshots them,
and locks the screen with the background images
passed through the 'convert' filters provided.

Each output's grim + convert instance is run in parallel

Examples:

	# scale-based pixelate for all HDMI outputs
	# pixelate more for eDP outputs (maybe high DPI?)
	$0 'HDMI*' -scale 20x20% -scale 500x500% -- 'eDP*' -scale 10%x10% -scale 1000%x1000%

	# sample-based pixelate and desaturate [ DEFAULT ]
	-sample 20x20% -modulate 100,50 -sample 500x500%

	# blur
	$0 -blur 2x8
"
	exit 0
	;;
esac

dir="$(mktemp -d)"
trap '{ rm -r "${dir?}"; return $?; }' INT
swaymsg -t get_outputs | jq -r '.[]|select(.active).name' | {
	while read -r output; do
		# ppm is a bitmapped format supported by grim, convert, and swaylock
		img="$(mktemp -p "$dir" --suffix=.ppm)"
		(
			grim -o "$output" "$img"
			convert "$img" "$@" "$img"
		) &
		lock_args="--image=$output:$img
$lock_args"
	done
	wait

	# suppress globbing, use newlines to split swaylock args
	set -f
	#shellcheck disable=2086
	IFS='
' swaylock $lock_args
}
