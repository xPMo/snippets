#!/usr/bin/env bash
# https://redd.it/b6u7ml
#shellcheck disable=1091
source "/path/to/key-bindings.bash"
# set cd binding to Alt-O
#shellcheck disable=2016
bind '"\eo": " \C-e\C-u`__fzf_cd__`\e\C-e\er\C-m"'
# overwrite Alt-C binding
bind '"\ec": capitalize-word'
