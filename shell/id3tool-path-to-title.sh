#!/usr/bin/env bash
IFS=$'\n'
set -o globstar
for f in **; do
	[[ -d "$f" ]] && continue
	id3tool "$f" -t "$(basename "$f" | cut -d- -f3 | sed 's/\.[^\,]*$//')"
done
