#!/usr/bin/env bash
declare -a tail_args
for arg in "$@"; do
	case $arg in
	[xX] | [xX]sess*) tail_args+=("-f" "$HOME/.xsession-errors") ;;
	foo) tail_args+=("-f" "path/to/foo.txt") ;;
	*) [[ -f "$arg" ]] && tail_args+=("-f" "$arg") ;;
	esac
done
tail "${tail_args[@]}"
