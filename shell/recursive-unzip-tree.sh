#!/usr/bin/env bash
# compressed: $1
# uncompressed: $2
c="$(realpath "$1")"
u="$(realpath "$2")"
set -o globstar

cd "$c" || exit 1
dirs=(**/) # copy tree
cd "$u" || exit 1
mkdir -p "${dirs[@]}"

cd "$u" || exit
for f in "$c/"**/*.gz; do
	gunzip -v "$f" -c >"$2/${f%%.gz}"
done
