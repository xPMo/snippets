#!/usr/bin/env bash
# tree >&2 if multiple
# show >&1 if single
[[ $# -eq 0 ]] && die "Usage: $PROGRAM $COMMAND pass-names..."
terms="*$(printf '%s*|*' "$@")"
find_matches="$(find "$PREFIX" -path "$PREFIX/${terms%|*}.gpg")"
count="$(wc -l <<<"$find_matches")"
find_matches="${find_matches#$PREFIX}"
case "${count%% *}" in
0) exit 1 ;;
1) cmd_show "${find_matches%.gpg}" ;;
*) tree -C -l --noreport -P "${terms%|*}" --prune --matchdirs --ignore-case "$PREFIX" | tail -n +2 | sed -E 's/\.gpg(\x1B\[[0-9]+m)?( ->|$)/\1\2/g' >&2 ;;
esac
