#!/bin/sh

_req(){
	curl -H "Authorization: Bearer ${MATRIX_ACCESS_TOKEN:?}" -H 'Content-Type: application/json' \
	"$@" "${MATRIX_HOMESERVER:-https://colony.jupiterbroadcasting.com}/_matrix/client/$schema"
}

_newkey(){
	date +'m%s.%N'
}

_resolve_alias(){
	room=
	case $1 in
		'#'*)
			room=$(schema=v3/directory/room/$1 _req | jq -r '"\(.roomID):\(.servers[0])"') || return 1
		;;
		'!'*:*) room=$1 ;;
		*) return 1
	esac
}

send(){
	# send a simple m.text message to the target room
	msg=${1:?missing message}
	_resolve_alias "${2:?missing roomID}" || return 1
	schema=v3/rooms/$room/send/${3:-m.room.message}/$(_newkey)
	_req -X PUT --data "$(jq --arg body "$msg" -n '{"msgtype":"m.text","body":$body}')"
}

state(){
	# list the state events in the given room
	_resolve_alias "${1:?missing roomID}" || return 1
	schema=v3/rooms/$room/state
	_req
}

"$@"
