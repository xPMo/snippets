#!/usr/bin/env bash
# args: [unprocessed dir] [processed dir]
set -o globstar
set -e
outdir="$(realpath "$2")"
cd "$1"
for file in **/*.mkv; do
	# unprocessed => processed
	outfile="$outdir/$file"
	# make path if it doesn't exist
	mkdir -p "$(dirname "$outfile")"
	# if mkvmerge succeeds, remove original
	mkvmerge -o "$outfile" --no-subtitles "$file" && rm "$file"
done
