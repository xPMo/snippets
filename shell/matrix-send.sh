#!/bin/sh
curl -X PUT -H "Authorization: Bearer ${MATRIX_ACCESS_TOKEN:?}" -H 'Content-Type: application/json' \
	--data "$(jq --arg body "${1:?missing message}" -n '{"msgtype":"m.text","body":$body}')" \
	"${MATRIX_HOMESERVER:-https://colony.jupiterbroadcasting.com}/_matrix/client/v3/rooms/${2:?missing roomID}/send/${3:-m.room.message}/$(date +'m%s.%N')"
