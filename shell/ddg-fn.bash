#!/usr/bin/env bash
# https://redd.it/b8u2c1
ddg() {
	set -- "${@/#\%/\!}" # replace leading % with ! in all args
	#firefox "https://duckduckgo.com/?q=$*"
	xdg-open "https://duckduckgo.com/?q=$*"
}
