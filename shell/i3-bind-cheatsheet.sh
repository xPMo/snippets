#!/usr/bin/env sh
i3-msg -t get_config |
	sed -ne '/^bind/{
	s/--no-startup-id //;
	s/--whole-window //;
	s/--release //;
	s/exec //g;
	s/bindsym //p}'
