#!/bin/sh
# https://redd.it/b7nooz
# use with dynamic-mpd-host.sh

MPD_HOST_FILE="${XDG_RUNTIME_DIR:-/tmp}/current-mpd-host"
MY_MPD_HOST="$(cat "$MPD_HOST_FILE")"
if [ "$MY_MPD_HOST" = "10.10.2.24" ]; then
	echo 127.0.0.1 >"$MPD_HOST_FILE"
	notify-send "Benutze 127.0.0.1 als mpd Host."
elif [ "$MY_MPD_HOST" = "127.0.0.1" ]; then
	echo 10.10.2.24 >"$MPD_HOST_FILE"
	notify-send "Benutze 10.10.2.24 als mpd Host."
else
	notify-send "Es gab ein Problem beim MPD Switchen."
fi
