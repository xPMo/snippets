#!/usr/bin/env bash
#https://redd.it/fjsr8v
find . -type f -printf '%f/%p\0' | {
	sort -z -t/ -k1
	printf '\0'
} |
	while IFS=/ read -r -d '' name file; do
		if [[ "$name" = "$oldname" ]]; then
			repeated+=("$file") # duplicate file
			continue
		fi
		if ((${#repeated[@]} > 1)); then
			printf '%s\n' "$oldname" "${repeated[@]}" ''
			# do something with list "${repeated[@]}"
		fi
		repeated=("$file")
		oldname=$name
	done
