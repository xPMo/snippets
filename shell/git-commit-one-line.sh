#!/usr/bin/env bash
# https://redd.it/b33512
mapfile <"$1"
change=0
for i in "${!MAPFILE[@]}"; do
	case ${MAPFILE[i]} in
	'+++'* | '---'*) continue ;;
	'+'*) # remove line
		((change++)) && unset 'MAPFILE[i]'
		;;
	'-'*) # replace first character with space
		((change++)) && MAPFILE[i]=" ${MAPFILE[i]:1}"
		;;
	esac
done

printf '%s' "${MAPFILE[@]}" >"$1"
